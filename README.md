# Preface

A backend with public & admin APs separation.

# Endpoints

curl http://localhost:9590/api/user/ping
curl http://localhost:9591/api/admin/ping

# Commands

## Starting the application

`npm start`

## Generating configuration

`npm run config:gen`

# Docker

## Building an image

`./docker_build.sh`

## Testing the container
`./docker_run.sh`

## Pushing the image to a repo
docker tag cyborg-public-api:v1 repo.corp.indexia.co:5000/cyborg-public-api:v1
docker push repo.corp.indexia.co:5000/cyborg-public-api:v1

## Updating version

Change in `docker_build.sh` the version and then rebuild.
Re-tag, re-push (see "Pushing the image to a repo")

# Kubernetes

## Schedule as a pod

---------TODO
#TODO: How do we expose two ports? probably we need a YAML/JSON manifest of the RC.
kubectl run cyborg-api --image=repo.corp.indexia.co:5000/cyborg-public-api:v1 --port=9590
kubectl get rc

kubectl expose rc cyborg-api --port=9590 --external-ip=10.10.0.6 --type=LoadBalancer
kubctl describe pod hello-node-xxxxx
---------

# ES

Initialize the DB with some data:

```bash
curl -X PUT 'http://localhost:9200/blog/user/spider_man' -d '{ "name" : "Spider Man" }'
curl -XPUT 'http://localhost:9200/blog/post/1' -d '{"user": "spider_man", "body": "With great power, comes great responsibility", "title": "Great Power"}'
curl -XPUT 'http://localhost:9200/blog/post/2' -d '{"user": "spider_man", "body": "I believe there is a hero in all of us that keeps us honest, give us strength, makes us noble.", "title": "I believe"}'
curl -X PUT 'http://localhost:9200/blog/user/dilbert' -d '{ "name" : "Dilbert Brown" }'
curl -XPUT 'http://localhost:9200/blog/post/3' -d '{"user": "dilbert", "body": "One day, I realized that \"Sadness\" is just another word for \"not enough coffee\"" ", "title": "Sadness and Coffee"}'
curl -XPUT 'http://localhost:9200/blog/post/4' -d '{"user": "dilbert", "body": "Dilbert, for search again...", "title": "Dilbert on search again"}'
curl 'http://localhost:9200/blog/post/_search?q=user:dilbert&pretty=true'
```