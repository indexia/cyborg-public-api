FROM node:6.2
MAINTAINER Asaf Shakarzy <asaf@indexia.co>
LABEL Description="Cyborg Public API" Vendor="Indexia Technologies, ltd." Version="0.0.1"

# Environment
ENV NODE_ENV=production

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 9590 9591
CMD [ "npm", "start" ]
