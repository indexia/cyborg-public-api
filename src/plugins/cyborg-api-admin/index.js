'use strict';

exports.register = function (server, options, next) {
  server.route({
    method: 'GET',
    path: '/ping',
    handler: (req, rep) => {
      rep('admin:pong!');
    }
  })
  next();
};

exports.register.attributes = {
  pkg: require('./package.json')
};
