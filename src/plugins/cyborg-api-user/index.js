'use strict';

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: `${process.env.ES_HOST}:9200`,
  log: 'trace'
});

exports.register = function (server, options, next) {
  server.route({
    method: 'GET',
    path: '/',
    config: {
      cors : true
    },
    handler: (req, rep) => {
      client.search({
        index: 'blog',
        type: 'user'
      }).then(function (body) {
        const { hits } = body;
        rep(hits);
      }, function (error) {
        rep(error.message).code(error.status);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/{user}',
    handler: (req, rep) => {
      client.get({
        index: 'blog',
        type: 'user',
        id: req.params.user
      }).then(function (body) {
        const { _source } = body;
        rep(_source);
      }, function (error) {
        rep(error.message).code(error.status);
      });
    },
    config: {
      cors : true
    }
  });

  server.route({
    method: 'GET',
    path: '/{user}/posts',
    handler: (req, rep) => {
      client.search({
        index: 'blog',
        type: 'post',
        q: `user:${req.params.user}`
      }).then(function (body) {
        const { hits } = body;
        rep(hits);
      }, function (error) {
        rep(error.message).code(error.status);
      });
    },
    config: {
      cors : true
    }
  });

  server.route({
    method: 'GET',
    path: '/ping',
    handler: (req, rep) => {
      rep('user:pong!!@');
    }
  })
  next();
}

exports.register.attributes = {
  pkg: require('./package.json')
};
