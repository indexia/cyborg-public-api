#!/bin/bash -e
if [ "$IS_PULL_REQUEST" != true ]; then
  sudo docker push $IMAGE_NAME:$BRANCH.$BUILD_NUMBER
else
  echo "skipping because it's a PR"
fi

# docker commit $SHIPPABLE_CONTAINER_NAME https://830271068349.dkr.ecr.us-east-1.amazonaws.com/cyborg/cyborg-api:preview
